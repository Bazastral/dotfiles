# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples


# If not running interactively, don't do anything
case $- in
  *i*) ;;
  *) return;;
esac


# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
  xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes
if [ -n "$force_color_prompt" ]; then
  if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    # We have color support; assume it's compliant with Ecma-48
    # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
    # a case would tend to support setf rather than setaf.)
    color_prompt=yes
  else
    color_prompt=
  fi
fi

if [ "$color_prompt" = yes ]; then
  PS1='${debian_chroot:+($debian_chroot)}\[\033[00;35m\]\u@\h\[\033[00m\]:\[\033[01;35m\]\w\[\033[00m\]\$ '
else
  PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# allow to see man in vim
export PAGER="/bin/sh -c \"unset PAGER;col -b -x | \
    vim -R -c 'set ft=man nomod nolist' -c 'map q :q<CR>' \
    -c 'map <SPACE> <C-D>' -c 'map b <C-U>' \
    -c 'nmap K :Man <C-R>=expand(\\\"<cword>\\\")<CR><CR>' -\""

export TERM=screen-256color

# If this is an xterm set the title to user@host:dir
case "$TERM" in
  xterm*|rxvt*)
    PS1="\[\033]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
  *)
    ;;
esac

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

today() {
  echo -n "Today's date is: "
  date +"%A, %B %-d, %Y"
}

IP_ADDR=$(hostname -I 2>/dev/null| awk '{print $1}')
if [ -z "$IP_ADDR" ]; then
    IP_ADDR="\h"
fi

# load colors
# each machine can have its own configuration
# if no configuration - fallback to default
if [ -f ~/.bash_colors ]; then
  . ~/.bash_colors
else
  TMUX_COL=17
  PS_MACHINE_COLOR=33
  PS_USR_COLOR=33
fi

#add important pre/suff
PS_MACHINE_COLOR="$PS_MACHINE_COLOR"m
PS_USR_COLOR="$PS_USR_COLOR"m
TMUX_COL=colour"$TMUX_COL"


PS_RESET='\[$(tput sgr0)\]'
PS_USER="\[\033[38;5;$PS_USR_COLOR\]\u"
PS_AT='\[\033[38;5;7m\]@'
PS_MACHINE="\[\033[38;5;$PS_MACHINE_COLOR\]$IP_ADDR"
PS_DIR='\[\033[38;5;7m\]\w'
PS_GIT='\[\033[38;5;202m$(__git_ps1)\]'
PS_NEWLINE='\n'
PS_PROMPT='\$'

function prompt
{
  export PS1="\
$PS_RESET\
$PS_USER\
$PS_AT\
$PS_MACHINE \
$PS_DIR \
$PS_GIT\
$PS_RESET\
$PS_NEWLINE\
$PS_PROMPT"
}
prompt


export SHELL=/bin/bash

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
LS_COLORS="$LS_COLORS:ow=1;33"; export LS_COLORS
if [[ ! "$PATH" == *$HOME/dotfiles/bin ]]; then
 export PATH="$PATH:$HOME/dotfiles/bin"
fi

export PATH=$HOME/bin:$PATH
export TMUX_COL

#eval "$(keychain --eval ~/.ssh/id_rsa)"

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
  test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
  alias ls='ls --color=auto'

  alias grep='grep --color=auto'
  alias fgrep='fgrep --color=auto'
  alias egrep='egrep --color=auto'
fi

alias tmux='tmux -2'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias tr="tree -L 3"
alias gs="git status"
alias gd="git diff"
alias gb="git branch -al"

