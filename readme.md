

Dotfiles
===

New user configuration files and install scripts

Steps
----
1. Clone repo into home directory (use https, not ssh)
```bash
git clone https://gitlab.com/Bazastral/dotfiles.gitlab
```
2. Install everything
```bash
sudo apt-get update
sudo apt-get upgrade
./install.sh
. ~/.profile
```
3. Check git user name and mail in ~/.gitconfig
4. Optionally change machine colors ~/.bash_colors
5. Manage ssh keys
```bash
ssh-copy-id -i .ssh/id_rsa.pub <uname>@<server ip>
```

6. Optionally setup keychain
