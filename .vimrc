
" set terminal 256 color version
set t_Co=256

" Color column
set colorcolumn=80

" colorscheme settings
colorscheme desert

" Enable exit/write confirmation
set confirm

" Show numbers
set relativenumber
set number

"Syntax highlighting
syntax on

"Default indentation settings
set autoindent
set cindent
set tabstop=2 shiftwidth=2 expandtab

"Always show status bar
set laststatus=2

set ignorecase
"allow backspace to remove neline and indentation in insert mode
set backspace=indent,eol,start

" statusline options
set statusline+=%#warningmsg#
set statusline+=%*


"Additional mapping
"Repeat search
vnoremap // y/<C-R>"<CR>
"Center after jump
nnoremap <c-]> <c-]>z.
nnoremap <c-o> <c-o>z.
nnoremap <c-i> <c-i>z.
nnoremap <n> <n>z.
nnoremap <N> <N>z.

"Faster and smoother movement
nnoremap <c-y> 3<c-y>
nnoremap <c-e> 3<c-e>


"refresh ctags && autoformat whole file
nmap <F5> :retab <CR> mlggVG='l <CR>
nmap <F6> :!ctags -R *<CR> :w<CR>

"add line
nnoremap K A<CR><ESC>

"buffers navigation
nmap <C-j> :bprev<CR>
nmap <C-k> :bnext<CR>
nnoremap <C-a> <C-^>

"windows resizing
nnoremap <C-h> :vertical resize -10 <CR>
nnoremap <C-l> :vertical resize +10 <CR>

"reload config
command! Reload source ~/.vimrc

highlight LineNr ctermfg=DarkGrey
highlight ColorColumn ctermbg=Darkgrey
