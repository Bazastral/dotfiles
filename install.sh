#!/bin/bash


# remove old
rm -f ~/.bash_colors
rm -f ~/.bashrc
rm -f ~/.gitconfig
rm -f ~/.tmux.conf
rm -f ~/.vimrc
rm -f ~/.gitconfig

#make links
ln -s ~/dotfiles/.bashrc ~/.bashrc
ln -s ~/dotfiles/.tmux.conf ~/.tmux.conf
ln -s ~/dotfiles/.vimrc ~/.vimrc
ln -s ~/dotfiles/.gitconfig ~/.gitconfig

# this is copied so as user can change without changing git repo
cp ~/dotfiles/.bash_colors ~/.bash_colors

#other
mkdir -p ~/projects
